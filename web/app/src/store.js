import Vue from "vue";
import Vuex from "vuex";

import { extractData } from "@/helpers";

Vue.use(Vuex);

